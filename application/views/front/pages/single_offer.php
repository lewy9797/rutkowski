<div class="container">

  <p class="my-5 top_margin">

    <h2 class="h1-responsive font-weight-bold text-center mt-5 mb-5"><?= $single->title ?></h2>
    <div class="row mb-4">
      <div class="col-12 col-lg-6"><?= $single->short_description ?></div>
      <div class="col-12 col-lg-6 d-flex justify-content-center align-items-center">
        <img class="img-fluid" src="<?= base_url(). 'uploads/'. $single->photo ?>" alt="<?= $single->alt ?>">
      </div>
    </div>

    <div class="row">
      <div class="col-12 col-lg-6 col-md-6"><?= $single->description_1 ?></div>
      <div class="col-12 col-lg-6 col-md-6"><?= $single->description_2 ?></div>
    </div>


  </div>


</section>

</div>

