<div class="container">

	<p class="my-5 top_margin">

		<h2 class="h1-responsive font-weight-bold text-center my-5"><?= $single->title ?></h2>

		<div class="row justify-content-center">

			<?php foreach($videos as $video): ?>
				<div class="col-lg-4 col-md-6">
					<video class="video-fluid" controls>
						<source src="<?= base_url(). 'uploads/'.$video->src ?>" type="video/mp4" />
						</video>
					</div>
				<?php endforeach; ?>
			</div>

			<div class="row">
				<?php $i=1; foreach ($gallery as $photo):?>
					<div class="col-lg-4 col-md-6 mb-0 mt-5">

						<a class="elem" href="<?= images().$photo->photo ?>">
							<div class="view overlay rounded z-depth-2 mb-4 gallery-box <?php if($this->uri->segment(2) == 1 ) echo 'portfolio-1-'.$i; elseif($this->uri->segment(2)==2) echo 'portfolio-2-'.$i; ?>" style="background-image: url(<?= images().$photo->photo ?>); ">
							</div>
						</a>

					</div>
				<?php $i++; endforeach; ?>
			</div>


		</div>


	</section>

</div>