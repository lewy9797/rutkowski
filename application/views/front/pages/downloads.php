<div class="container">
	<section class="my-5 top_margin">

		<h2 class="font-weight-bold text-center header__text"><?= $current_page->subtitle ?></h2>

		<div class="text-center w-responsive mx-auto mb-5"><?= $current_page->description ?></div>

		<div class="row">
			<?php foreach ($downloads as $key): ?>
				<div class="col-md-12 mb-lg-4 mb-4">
					<div class="card collection-card z-depth-1-half">

							<div class="stripe dark" style="position: unset;">
								<a href="<?= base_url(). 'uploads/' . $key->path ?>">
									<p><?= $key->title; ?>
									<i class="fas fa-angle-right"></i>
								</p>
							</a>


					</div>
				</div>

			</div>
		<?php endforeach; ?>
	</div>

</section>
</div>