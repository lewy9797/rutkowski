<div class="container">
	<section class="my-5 top_margin">

      	<h2 class="font-weight-bold text-center header__text"><?php if(count($props) != 0) echo $current_page->subtitle; else echo 'Strona w budowie. Przepraszamy'; ?></h2>

      	<div class="text-center w-responsive mx-auto mb-5"><?= $current_page->description ?></div>

	    <div class="row">
	      	<?php foreach ($props as $key): ?>
		        <div class="col-lg-6 col-md-12 mb-lg-4 mb-4">
		            <div class="card collection-card z-depth-1-half" onclick="link('<?= base_url().'portfolio/'.$key->id.'/'.slug($key->title); ?>')">
				
		              <div class="view zoom offer-box webp" style="background-image: url(<?= images().$key->photo; ?>.webp);">

		                <div class="stripe dark">
		                  <a href="<?= base_url().'portfolio/'.$key->id.'/'.slug($key->title); ?>">
		                    <p><?= $key->title; ?>
		                      <i class="fas fa-angle-right"></i>
		                    </p>
		                  </a>
		                </div>
		              </div>

		          
		              <div class="view zoom offer-box no-webp" style="background-image: url(<?= images().$key->photo; ?>);">

		                <div class="stripe dark">
		                  <a href="<?= base_url().'portfolio/'.$key->id.'/'.slug($key->title); ?>">
		                    <p><?= $key->title; ?>
		                      <i class="fas fa-angle-right"></i>
		                    </p>
		                  </a>
		                </div>
		              </div>
		            </div>

		        </div>
		    <?php endforeach; ?>
	    </div>
	    
    </section>
</div>