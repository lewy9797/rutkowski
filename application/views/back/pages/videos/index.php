    <!-- ########## START: MAIN PANEL ########## -->
    <div class="br-mainpanel">
      <div class="pd-30">
        <h4 class="tx-gray-800 mg-b-5">Filmy do tabeli: 
          <strong><?php echo ucfirst(str_replace('_', ' ', $this->uri->segment(4))); ?></strong> #<?php echo $this->uri->segment(5); ?>
        </h4>
        <p class="mg-b-0"><?php echo subtitle(); ?></p>
        <hr>
      </div><!-- d-flex -->

      <div class="br-pagebody mg-t-0 pd-x-30">
        <?php if(isset($_SESSION['flashdata'])): ?>
          <div id="alert-box"><?php echo $_SESSION['flashdata']; ?></div>
        <?php endif; ?>

        <form class="form-layout form-layout-2" action="<?php echo base_url(); ?>panel/settings/videos_action/<?php echo $this->uri->segment(4) . '/' . $this->uri->segment(5); ?>" method="post" enctype="multipart/form-data">

          <div class="row no-gutters">
            <div class="col-md-8">
              <div class="row"> <!-- set -->
                <div class="col-md-12 pr-0">
                  <div class="form-group">
                    <div id="update"></div>
                    <label class="form-control-label">Filmy:</label>
                    <div class="row align-items-center">
                      <?php foreach ($rows as $value): ?>
                        <div class="col-lg-4 col-md-6 text-center mb-3 align-self-center">
                          <video class="img-fluid" controls>
                            <source src="<?= base_url(). 'uploads/'.$value->src ?>" type="video/mp4" />
                            </video>
                            <a href="<?php echo base_url(); ?>panel/settings/delete_videos/<?php echo $this->uri->segment(4); ?>/<?php echo $value->id; ?>" class="input-group-addon d-flex justify-content-between text-center tx-size-sm lh-2" title="usuń film">
                              Usuń
                              <i class="fa fa-close"></i>
                            </a>
                          </div>
                        <?php endforeach; ?>
                      </div>
                    </div>
                  </div><!-- col-4 -->
                </div> <!-- set -->
              </div>
              <div class="col-md-4">
                <div class="col-md-12">
                  <div class="form-group bd-l-0-force">
                    <div id="infoGallery"></div>
                    <label id="photosLabel" class="form-control-label">Filmy:</label>
                    <label class="custom-file">
                      <input id="length" type="file" class="custom-file-input" name="videos[]" onchange="uploadVideos()" multiple="multiple">
                      <span class="custom-file-control custom-file-control-inverse"></span>
                    </label>
                  </div>
                </div><!-- col-12 -->
                <div class="col-md-12">
                  <div class="form-layout-footer bd pd-20 bd-l-0-force bd-t-0-force">
                    <button class="btn btn-info" type="submit">Zapisz</button>
                    <button class="btn btn-secondary" onclick="window.history.go(-1); return false;">Anuluj</button>
                  </div><!-- form-group -->
                </div>
              </div>
            </div><!-- row -->
          </form><!-- form-layout -->

          <script type="text/javascript">
            function uploadVideos() {
              document.getElementById('infoGallery').innerHTML = '<span class="text-center"><i class="fas fa-spinner fa-pulse loader"></i></span>';
              setTimeout(function(){ 
                document.getElementById('infoGallery').innerHTML = '<span class="text-success"><i class="fas fa-check"></i> Filmy zostały przygotowane do wysłania!</span>';
              }, 1500);
            }
          </script>